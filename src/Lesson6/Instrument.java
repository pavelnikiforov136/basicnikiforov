package src.Lesson6;

public abstract class Instrument {
    //Создать абстрактный класс Инструмент и наследующие от него классы Гитара, Барабан и Труба.
    //Инструмент содержит абстрактный метод play() и переменную String KEY ="До мажор".
    private String KEY ="До мажор";
    abstract void play ();
}