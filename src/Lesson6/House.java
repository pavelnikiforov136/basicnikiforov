package src.Lesson6;

public class House {
    /**
     *     Создайте класс дом, в котором создайте 2 объекта от каждого класса и вызовите все методы,
     *     которые каждый из них умеет.
     */
    private CounterStrike counterStrike1 =new CounterStrike();
    private   CounterStrike counterStrike2 =new CounterStrike();
    private   SnakesAndLadders snakesAndLadders1 = new SnakesAndLadders();
    private SnakesAndLadders snakesAndLadders2 = new SnakesAndLadders();

    public void StarGame (){
        counterStrike1.start();
        counterStrike1.move();
        counterStrike1.shoot();
        counterStrike1.end();
        counterStrike2.start();
        counterStrike2.move();
        counterStrike2.shoot();
        counterStrike2.end();


        snakesAndLadders1.start();
        snakesAndLadders1.rollDice();
        System.out.println("Игровой статус кубика : "+snakesAndLadders1.end());
        snakesAndLadders2.start();
        snakesAndLadders2.rollDice();
        System.out.println("Игровой статус кубика : "+snakesAndLadders2.end());
    }
}
