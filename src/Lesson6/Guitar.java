package src.Lesson6;

public class  Guitar extends Instrument {
    //Гитара содержит переменные класса количествоСтрун,  Барабан - размер, Труба - диаметр.
    int countString=7;

    @Override
    void play() {
        System.out.println("Играет  Guitar, с количеством струн: " + countString);
    }
}
