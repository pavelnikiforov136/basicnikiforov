package src.Lesson6;

public class Drums extends Instrument{
    // //Гитара содержит переменные класса количествоСтрун,  Барабан - размер, Труба - диаметр.
    double size = 60;
    @Override
    void play() {
        System.out.println("Играет Drums, из натуральной кожи, " + size + " см,  в диаметре");
    }
}
