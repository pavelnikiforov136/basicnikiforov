package src.Lesson6;

public interface ComputerGame extends Game{
    //    Создайте 3 интерфейса, Game, TableGame extends Game, ComputerGame extends Game.
    void shoot();
    void move();
}
