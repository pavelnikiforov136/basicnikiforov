package src.Lesson6;

import java.util.Random;

public class CounterStrike implements ComputerGame{
    /**
     *     Создайте 2 класса - реализации:
     *         SnakesAndLadders implements TableGame
     *         CounterStrike implements ComputerGame
     *     Каждый из классов должен релизовать все методы своего интерфейса.
     *     Объекты класса настольные игры не должны уметь нажимать на кнопку,
     *     объекты класса компьютерные игры не должны уметь бросать кубик.
     */

    private String name;
    private boolean isActive;

    CounterStrike() {
        Random r = new Random();
        String alphabet  = "123xyzjhgfhweghfjewg87987798";
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < 10; i++) {
            stringBuilder.append(alphabet .charAt(r.nextInt(alphabet.length())));
        }
        name= stringBuilder.toString();
    }

    @Override
    public void shoot() {
        Random rand = new Random();
        int randomNum = rand.nextInt((1000 - 1) + 1) + 1;
        System.out.println(" Игрок с именем :" +name + " высьрелили из ShortGun: " + randomNum + " патронами");

    }

    @Override
    public void move() {
        Random rand = new Random();
        int randomNum = rand.nextInt((1000 - 1) + 1) + 1;
        System.out.println(" Игрок с именем :" +name + " переместился на растояние: " + randomNum + " м.");
    }

    @Override
    public void start() {
        System.out.println("Начинает играть игрок : "+name);
    }

    @Override
    public Boolean end() {
        return isActive;
    }
}
