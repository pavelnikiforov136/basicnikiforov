package src.Lesson6;

public class HomeWork6 {
    /*
Создать абстрактный класс Инструмент и наследующие от него классы Гитара, Барабан и Труба.
Инструмент содержит абстрактный метод play() и переменную String KEY ="До мажор".
Гитара содержит переменные класса количествоСтрун,  Барабан - размер, Труба - диаметр.
Создать массив типа Инструмент, содержащий инструменты разного типа.
В цикле вызвать метод play() для каждого инструмента,
который должен выводить строку "Играет такой-то инструмент, с такими-то характеристиками".
Создайте 3 интерфейса, Game, TableGame extends Game, ComputerGame extends Game.
Интерфейс Game должен содержать метод void start() и метод Boolean end().
Интерфейс TableGame должен содержать метод void rollDice().
Интерфейс ComputerGame должен содержать метод void shoot() и метод void move().
Создайте 2 класса - реализации:
    SnakesAndLadders implements TableGame
    CounterStrike implements ComputerGame
Каждый из классов должен релизовать все методы своего интерфейса.
Объекты класса настольные игры не должны уметь нажимать на кнопку,
объекты класса компьютерные игры не должны уметь бросать кубик.
Создайте класс дом, в котором создайте 2 объекта от каждого класса и вызовите все методы,
которые каждый из них умеет.
*/
    public static void main(String[] args) {
        Instrument [] myInstruments = new Instrument[3];
        myInstruments [0] = new Guitar();
        myInstruments [1] = new Drums();
        myInstruments [2] = new Trumpet();

        for (int i =0; i <= myInstruments.length -1 ; i++){
            myInstruments[i].play();
        }
        House  house  =  new House();
        house.StarGame();
    }

}
