package src.Lesson6;

public class Trumpet extends Instrument{
    //Гитара содержит переменные класса количествоСтрун,  Барабан - размер, Труба - диаметр.
    double diameter = 15;
    @Override
    void play() {
        System.out.println("Играет Trumpet, из натурального железа, " + diameter + " см,  в диаметре");
    }
}
