package src.Lesson6;

import java.util.Random;

public class SnakesAndLadders implements TableGame{
    /**
     *     Создайте 2 класса - реализации:
     *         SnakesAndLadders implements TableGame
     *         CounterStrike implements ComputerGame
     *     Каждый из классов должен релизовать все методы своего интерфейса.
     *     Объекты класса настольные игры не должны уметь нажимать на кнопку,
     *     объекты класса компьютерные игры не должны уметь бросать кубик.
     */

    private String name;
    private boolean isActive;

    SnakesAndLadders(){
        Random r = new Random();
        String alphabet  = "123xyzjhgfhweghfjewg87987798";
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < 10; i++) {
            stringBuilder.append(alphabet .charAt(r.nextInt(alphabet.length())));
        }
        name= stringBuilder.toString();
    }

    @Override
    public void start() {
        System.out.println("Начинаем бросать кубик с имененм : "+name);
    }

    @Override
    public Boolean end() {
        return isActive;
    }

    @Override
    public void rollDice() {
        Random rand = new Random();
        int randomNum = rand.nextInt((6 - 1) + 1) + 1;
        System.out.println("У кубика : "+ name + " выпало "+randomNum + " очков");
        isActive= false;
    }
}